Feature: Test the book store REST APIs
  As a consumer of the bookstore API
  I want to get a list of all the books
  I can create a user account on the book store

  Acceptance Criteria
  - User should be able to get a list of all the books without registering itself
  - User should be able to register itself
  - API should return appropriate error message when a secured data is requested
  
  @smoke
   Scenario: Get a list of all the books available on bookstore
    When I hit the GetAllBooks endpoint
    Then I should receive the the list of books as Json string
    And The response code of the Response should be 200
    
       
    @regression
   Scenario: User is able to register to the Bookstore
    Given I have a unique username and password
    When I hit the RegisterUser endpoint
    Then I should be able to register to the bookstore
    And The response code of the Response should be 200 
     
    