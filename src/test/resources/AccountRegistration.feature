Feature: Test the account registration functionality

  Acceptance Criteria
  - User should get the option to signin on the home page
  - User should be able to register a user account on the store website
  
  
  Scenario: Signin button should be displayed on the homepage
      Given I am on the store homepage and I am not signed in
      Then I should get an option to signin
      
      
	@smoke
  Scenario: User is able to register an account sucessfully on the store
      Given I am on the store homepage and I am not signed in
      When I click on the Signin button
      Then I should get an option to register a new email ID
      When I provide all the personal details
      Then I am able to successfully register a new account
 