package stepDefinitions;

import org.json.simple.JSONObject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class BookStoreSteps {
	
	private Response response = null;
	
	@When("I hit the GetAllBooks endpoint")
	public void i_hit_the_GetAllBooks_endpoint() {
		
	}

	@Then("I should receive the the list of books as Json string")
	public void i_should_receive_the_the_list_of_books_as_Json_string() {
		
	}

	@Then("The response code of the Response should be {int}")
	public void the_response_code_of_the_Response_should_be(Integer int1) {
		
	}

	
	
	// Scenario 2

	JSONObject requestBody = null;
	
	@Given("I have a unique username and password")
	public void i_have_a_unique_username_and_password() {
		
	}
	
	@When("I hit the RegisterUser endpoint")
	public void i_hit_the_RegisterUser_endpoint() {
		
	}


	@Then("I should be able to register to the bookstore")
	public void i_should_be_able_to_register_to_the_bookstore() {
		
	}
	
}
