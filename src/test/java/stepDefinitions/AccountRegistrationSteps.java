package stepDefinitions;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AccountRegistrationSteps {
	WebDriver driver;
	
	@Given("I am on the store homepage and I am not signed in")
	public void i_am_on_the_store_homepage_and_I_am_not_signed_in() {
		
	}

	@Then("I should get an option to signin")
	public void i_should_get_an_option_to_signin() {

	}

	@When("I click on the Signin button")
	public void i_click_on_the_Signin_button() {
		
	}

	@Then("I should get an option to register a new email ID")
	public void i_should_get_an_option_to_register_a_new_email_ID() {

	}

	@When("I provide all the personal details")
	public void i_provide_all_the_personal_details() {

	}

	@Then("I am able to successfully register a new account")
	public void i_am_able_to_successfully_register_a_new_account() {

	}

}
