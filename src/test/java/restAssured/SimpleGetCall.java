package restAssured;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class SimpleGetCall {

	public static void main(String[] args) {
		// https://bookstore.toolsqa.com

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Associate a URL to the RestAssured object
		 */
		RestAssured.baseURI = "https://bookstore.toolsqa.com";
		
		/*
		 * Get a RequestSpecification for the above URL
		 */
		RequestSpecification httpRequest = RestAssured.given();
		
		/*
		 * Execute the request and wait till the Response is received
		 */
		Response response = httpRequest.request(Method.GET, "/BookStore/v1/Books");
		
		/*
		 * Get the Response status code and print
		 */
		int statusCode = response.getStatusCode();
		System.out.println("The status code of the response code: " + statusCode);
		
		/*
		 * Get the response body and print
		 */
		ResponseBody body = response.getBody();
		body.prettyPrint();
		
	}

}
