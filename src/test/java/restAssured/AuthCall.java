package restAssured;

import java.util.Base64;
import org.json.simple.JSONObject;
import bookStore.UserAccount;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AuthCall {

	public static void main(String[] args) {
		RestAssured.baseURI = "https://bookstore.toolsqa.com";
		//RestAssured.baseURI = "toolsqa-bookstore.azurewebsites.net";
		RequestSpecification createUserRequest = RestAssured.given();		

		String userId = String.valueOf((int)(Math.random() * 10000));
		JSONObject requestBody = new JSONObject();
		requestBody.put("userName", "sampleUser"+ userId);
		requestBody.put("password", "TestPassword1@");
		
		createUserRequest.header("Content-Type", "application/json");
		createUserRequest.body(requestBody.toJSONString());
		
		Response createUserResponse = createUserRequest.request(Method.POST, "/Account/v1/User");
		UserAccount userAccount = createUserResponse.body().jsonPath().getObject("$", UserAccount.class);
				
		
		String authString = userAccount.userName + ":" + "TestPassword1@";
		byte[] base64Encoded = Base64.getEncoder().encode(authString.getBytes());
		String encodedString = new String(base64Encoded);
		
		RequestSpecification deleteUserRequest = RestAssured.given();
		deleteUserRequest.header("Content-Type", "application/json");
		deleteUserRequest.header("Authorization", "Basic " + encodedString); 
		
		Response deleteResponse = deleteUserRequest.request(Method.DELETE, "/Account/v1/User/"+ userAccount.userID);
		System.out.println("STATUS CODE FOR DELETE REQUEST : "+deleteResponse.getStatusCode());
	}

}
