package restAssured;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SimplePostCall {

	public static void main(String[] args) {
		/*
		 * This code makes a simple Post call to the Endpoint: http://bookstore.toolsqa.com/Account/v1/User
		 * This endpoint will enable us to create a new user on the API store. Using the new user we will
		 * add books to the account
		 * 
		 * Expected request body
		 * 
		 * {
		 *	  "userName": "string",
		 *	  "password": "string"
		 * }
		 */
		
		
		/*
		 * Specify the endpoint URL
		 */
		RestAssured.baseURI = "https://bookstore.toolsqa.com";
		
		/*
		 *  Get the Request specification
		 */
		RequestSpecification request = RestAssured.given();
		
		/*
		 * Create a Json request body
		 */

		JSONObject requestBody = new JSONObject();
		requestBody.put("userName", "sampleUser124");
		requestBody.put("password", "TestPassword1@");
		
		/*
		 * Specify in the request that we will be sending a json string.
		 * With this the server will know that it should interpret the
		 * String as Json
		 */
		request.header("Content-Type", "application/json");
		
		/*
		 * Add the json to the request body. This can be done
		 * by calling request.body method
		 */
		request.body(requestBody.toJSONString());
		
		/*
		 * Fire the request
		 */
		Response response = request.request(Method.POST, "/Account/v1/User");
		
		/*
		 * Print the status code and Response of the request
		 */
		System.out.println("Response status code: " + response.getStatusCode());
		System.out.println("Response body: " + response.getBody().asString());
	}

}
