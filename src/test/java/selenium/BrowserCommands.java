package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserCommands {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Java Projects\\Libraries\\Drivers\\chromedriver.exe");

		
		  WebDriver driver = new ChromeDriver(); 

		
		  driver.get("http://toolsqa.com");
		  
		  String title = driver.getTitle();
		  System.out.println("Title of the website is :" + title);
		  
		  String currentUrl = driver.getCurrentUrl();
		  System.out.println("Current URL opened is :" + currentUrl);
		  
		  
		  String pageSource = driver.getPageSource();
		  System.out.println("Page source of the current page is :" + pageSource);
		  
		  
		  driver.quit();
		 
		 
	}

}
