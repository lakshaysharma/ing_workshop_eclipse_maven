package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ElementCommand {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Java Projects\\Libraries\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.toolsqa.com/automation-practice-form/");
	  
		WebElement element = driver.findElement(By.name("firstname"));
		element.sendKeys("Lakshay");
	  
	    driver.findElement(By.cssSelector("[name='lastname']")).sendKeys("Sharma");
	    driver.findElement(By.xpath("//input[contains(@name,'firstname')]")).clear();
	    driver.close(); 
	}

}
