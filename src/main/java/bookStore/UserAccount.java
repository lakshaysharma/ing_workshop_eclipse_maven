package bookStore;

import java.util.ArrayList;

public class UserAccount {
	public String userID;
    public String userName;
    public ArrayList<Book> books;

    public class Book {
        public String isbn;
        public String title;
        public String subTitle;
        public String author;
        public String published;
        public String publisher;
        public String pages;
        public String description;
        public String website;
    }
}
